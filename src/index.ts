/*import "source-map-support/register";
import serverlessExpress from "@vendia/serverless-express";
import { app } from "./app";

export const handler = serverlessExpress({ app });*/
require('source-map-support/register')
import connectToDB from "./database/db";
const serverlessExpress = require('@vendia/serverless-express')
const { app } = require('./app')

connectToDB();

let serverlessExpressInstance: any

function asyncTask () {
  return new Promise((resolve) => {
    setTimeout(() => resolve('connected to database'), 1000)
  })
}

async function setup (event: any, context: any) {
  const asyncValue = await asyncTask()
  console.log(asyncValue)
  serverlessExpressInstance = serverlessExpress({ app })
  return serverlessExpressInstance(event, context)
}

exports.handler = (event:any , context: any) => {
  if (serverlessExpressInstance) return serverlessExpressInstance(event, context)

  return setup(event, context)
}

//exports.handler = (function) handler

/*const port = process.env.PORT || "5000";
/*app.listen(port, () => {
    logger.info(`Listen on ${port}`);
});*/
